# Flowcharts

<br>

**MiniX6 flowchart**

![](Flowchart-Grisse.png "billed titel")

<br>

**Idea 1 - Extinction is forever!**

![](Flowchart-Idea1.png "billed titel")

Our first idea is called “Extinction is forever” which is a game where you will be trying to save an endangered species of your choice. There will be three species to choose from. Each species will come with facts of why they are endangered and after this the player will face choices of how to save the animal. The choices matter and will define how long the species can survive!

<br>

**Idea 2 - Virtual friend**

![](Flowchart-Idea2.png "billed titel")

Our second idea is called “Virtual friend”, where the user is introduced to a “friend” initially appearing cute and friendly. It asks the user personal but innocent questions gaining the trust of the user. The questions slowly turn more invasive and ominous, while the user is unable to avoid or exit the program. Once all questions have been answered the screen crashes, revealing that the “friend” has left with your stolen data.

<br>

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level??**

A difficulty was needing to strike a balance of including enough information for understanding without overwhelming readers with unnecessary technical details. Technical terms might not translate well, and simplifying the steps too much can lead to confusion and misunderstandings.

<br>

**What are the technical challenges facing the two ideas and how are you going to address these?**

Both programs will include many steps or levels that can result in different outcomes.
Developing decision-making algorithms that respond dynamically to the answers and prioritize actions based on their impact on the animal's survival can be complex.
Creating interactive elements such as buttons, sliders, and visual feedback
And in relation to the other idea “Virtual friend” a challenge will be to focus on the social aspects of the friend and create a clear progression of trust being broken (data breaches, targeted ads etc.).
Difficult to create an animation for the virtual friend.

<br>

**In which ways are the individual and the group flowcharts you produced useful?**

The group flowcharts are useful in a way that it can be used in some sort of brainstorm. It is a great way to get ideas written down and to see the opportunities of the ideas. The individual flowchart is useful in another way. It is most useful in a way that it can help one to understand their miniX assignment by breaking down the code, and investigating how it works.
