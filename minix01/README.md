# Return Saturn

For my first miniX I created a code that visualizes a blue ellipse representing Earth with a moon besides it. I chose to incorporate an interactive feature, consisting of a button labeled "return". When the button is pressed, the color of the Earth changes to pink, rings are added to the moon intended to portray Saturn, as it moves to the opposite side of the Earth. The "return" button can be clicked repeatedly, indefinitely shifting between the two states of "the universe"/the program.

![](minix01screenshot.png "billed titel")
<br>

Please run the code [here](https://andet-semester1.gitlab.io/aep/minix01/minix01index.html)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

<br>
Please view the code [here](https://gitlab.com/andet-semester1/aep/-/blob/main/minix01/minix01sketch.js?ref_type=heads)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

By creating the variables let startScreen = true and let saturnVisible = false, and assigning them with a boolean value, I am able to keep track of the initial state of the start screen, the visibility of Saturn, enabling monitoring and manipulation of these conditions throughout the program. SaturnVisible is initially set to false, as that shall only appear when the button has been pressed. Let earthColor; will store the color of the earth ellipse, allowing me to later change the color of the Earth, while keeping the moon/Saturn ellipse a constant color.

In the setup() function I've created the canvas. The noStroke() function removes outlines from all the shapes. I assign colors for the Earth and the moon/Saturn. I create the button using the function createButton(), and position it at the coordinates (715, 417). A callback function named returnSaturn() is attached to it through the mousePressed event listener.

In the draw() function the background color is set. The stars are drawn first, as they should appear behind the Earth. Fill(); allows me to determine the color of the ellipses, while the coordinates determine the position and diameter. The Earth ellipse is initially filled with the blue color. Through an if statement, I determine that if saturnVisible is true, the variable earthColor is assigned a new value and saturn and it's rings are drawn using the function ellipse() and curve(). Else, if saturn is not visible the original color is assigned to the color variable and the moon is drawn. The translation () function allows me to change the moon's position in relation to Earth.

Underneath the draw() function I create the returnSaturn() function that set saturnVisible to either true or false, based on whether the button has already been pressed, updating the screen.

The code I made is inspired by Saturn's orbit around the sun, and the saying that once it returns to it's original position every 29.5 years, it symbolizes the end of one thing and new start of another. Once the 'return' button is clicked, the Earth as we know it is distorted and twisted, representing it's new beginning of something different and perhaps positive.

Learning to read and understand the coding language including the different rules and terms, while learning to implement it myself has been challenging so far. Through this code, I wanted to explore geometric shapes as I am getting to know p5.js. I wanted to focus on the shapes of ellipses, as stars and the planets of the solar system intrigue me. I knew I wanted to incorporate Saturn and it's ring, so additionally I explored the curve() function, playing with the noumerous koordinates it introduces. I introduced an interactive aspect to allow myself to explore conditional if statements, and assigning different values to a variable. Overall, I found this code challenging but fun to create. I got an idea for the concept, and then sought inspiration from others, in regards to brining the different aspects into existence. The exploration and understanding of examples made by other people, has made me able to expand upon my idea, and create something that I initially would've found very advanced and difficult to do in learning the coding language.

### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->

[Reference for Saturn inspiration](https://editor.p5js.org/22haskelld/sketches/91WIKUk1d)
<br>
[Reference for curve](https://p5js.org/reference/#/p5/curve)
<br>
[Reference for ellipse](https://p5js.org/reference/#p5/ellipse)
