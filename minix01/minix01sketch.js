let button; // Button to toggle between moon and Saturn

let earthColor; // Color of the Earth

let startScreen = true; // Variable to track the screen state

let saturnVisible = false; // Flag to check if Saturn is visible

function setup() {
    createCanvas(windowWidth, windowHeight); // Create a canvas that fits the window size

    earthColor = color(100, 149, 237); // Set the initial color of the Earth

    // Create a button to toggle between moon and Saturn
    button = createButton('uɹnʇǝɹ');
    button.position(715, 417); // Position the button
    button.mousePressed(returnSaturn); // Set the function to call on button press
}

function draw() {
    background(106, 90, 205); // Set the background color
    noStroke(); // Disable strokes for shapes

    // Draw stars
    fill(255); // Set color to white
    ellipse(400, 700, 5, 5);
    ellipse(350, 239, 5, 5);
    ellipse(500, 500, 5, 5);
    ellipse(500, 158, 5, 5);
    ellipse(900, 125, 5, 5);
    ellipse(1000, 400, 5, 5);
    ellipse(589, 277, 5, 5);
    ellipse(4000, 136, 5, 5);
    ellipse(900, 700, 5, 5);

    // Draw the Earth
    fill(earthColor); // Use the current Earth color
    ellipse(730, 430, 400); // Position and size of the Earth

    if (saturnVisible) {
        earthColor = color(255, 105, 180); // Change Earth's color when Saturn is visible

        // Draw Saturn
        fill(245, 255, 250); // Set color for Saturn
        translate(350, 0); // Translate the coordinate system for Saturn's position
        ellipse(50, 430, 100); // Draw Saturn

        stroke(255, 0, 255); // Set stroke color for rings
        strokeWeight(2); // Set stroke weight for rings
        noFill(); // No fill for rings

        // Draw Saturn's rings using curves
        curve(-30, 300, -30, 430, 125, 430, 100, 300);
        curve(-30, 450, 4, 410, -30, 430, 30, 450);
        curve(-30, 450, 125, 430, 97, 410, 100, 450);

    } else {
        earthColor = color(100, 149, 237); // Change Earth's color when the moon is visible
        fill(245, 255, 250); // Set color for the moon
        translate(350, 0); // Translate the coordinate system for the moon's position
        ellipse(730, 430, 100); // Draw the moon
    }
}

function returnSaturn() {
    //If button is pressed when original startscreen is displayed
    if (startScreen) {
        // Show Saturn, hide the moon and set startscreen to false
        startScreen = false;
        saturnVisible = true;
        
    //If button is pressed when original startscreen is not displayed
    } else if (!startScreen) {
        // Show the moon, hide Saturn, and set startscreen to true
        startScreen = true;
        saturnVisible = false;
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight); // Resize the canvas when the window is resized
}