# Jelly Belly

<br>

![](jellyscreenshot.png "billed titel")

<br>

Please run the code [here](https://astrid-ap.gitlab.io/aestetisk-programmering/MiniX08)

Please view the code [here](https://gitlab.com/astrid-ap/aestetisk-programmering/-/blob/main/MiniX08/sketchminix08.js)

<br>

**What is the program about? Which API have you used and why?**

Our program generates a random Jelly Belly Bean and displays information about it, including its flavor name and description, along with an image of the bean. BeanId is also used but not displayed. The beanId determines which image, background color and text color are displayed for the selected Jelly Belly Bean. The API we use is from Jelly Belly Wiki (https://jellybellywikiapi.onrender.com/api/beans), which gives us information about the different flavors. We used this API because we thought it would bring joy and color to whoever uses our program.

<br>

**Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data?**

We acquired the data from the external API (https://jellybellywikiapi.onrender.com/api/beans). In the “preload” function we used the loadJSON() function to fetch data from the API asynchronously.
Once the data is retrieved, the gotData() function processes it. It selects a random item from the list of beans and extracts relevant information such as bean ID, flavor name, and description. The data is represented visually using images and text on the canvas. Each jelly bean flavor is visually represented by its image, flavor name, and description. Among the information we chose to leave out is whether the bean is sugar-free, gluten-free, seasonal or kosher.

<br>

**How much do you understand this data or what do you want to know more about?**

It is significant to understand the structure and content of the data and inspect how it is organized, to manipulate and display it correctly. The option to check a “pretty print” option, optimizes the readability of the data. The data is relatively easy to understand, due to the properties being assigned simple, recognizable names such as “flavourName”.

<br>

**How do platform providers sort the data and give you the requested data?**

Platform providers may sort and provide data based on various criteria, such as popularity, relevance, or randomness, depending on the API's design and purpose. Sorting algorithms or techniques may be employed by the provider to ensure fair and diverse data retrieval.

<br>

**What is the significance of APIs in digital culture?**

The significance of APIs in digital culture is that we need to understand that all the data provided online is (specifically) selected by the programmers and the business company. When using an API some data is selected while some is absent, which means that we don’t get all the information acquired, but we only get the information that the programmer and company want us to acquire. The point is: Nothing is randomly created, everything is specifically chosen by the creators. For instance, in our project, we have access to all the information about the Jelly Belly Bean like what kinds of beanId the beans contain and are they sugar free or gluten free. For our project we wanted to gather knowledge about, what their flavor name was and the description of the Jelly Belly Bean.

<br>

**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**

Are there any ways to find out if a specific website uses an API or multiple APIs?

### **References**

[Reference](https://www.youtube.com/watch?v=rJaXOFfwGVw&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&ab_channel=TheCodingTrain)
