# Throbber

<br>

![](minix3Screeenshot.png "billed titel")

<br>

Please run the code [here](https://andet-semester1.gitlab.io/aep/minix03/index.html)

Please view the code [here](https://gitlab.com/andet-semester1/aep/-/blob/main/minix03/sketch.js)

<br>

**Describe your throbber design, both conceptually and technically.**

My throbber resembles a colorful bird spinning around in a circle, increasing in speed as time passes.
The bird represents the saying that "time flies", which I found relevant to include in the context of throbbers, where users are usually confronted with feelings of impatience or boredom. In the background white clouds are passing by, floating across the sky from left to right. A tree is drawn to the left of the throbber. It changes color and style continuously, representing the natural cycle of seasons passing.

<br>

**What do you want to explore and/or express?**

I wanted to explore a variety of motions and cycles. The bird orbiting, the clouds drifting horizontally, and the tree fading into new expressions, contribute to establishing a dynamic and engaging program. As the speed of the throbber accelerates, it eventually transforms into an aesthetically pleasing, symmetrical pattern of colors and shapes. The exploration of diverse movements aims to capture the attention and curiosity of the 'user', providing entertainment while waiting for something else to load. The shifting seasons of the tree, symbolize the concept that what may seem like hours and hours of waiting, is often just a brief moment in the grand scheme of things.

<br>

**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?**

This part of the code is responsible for moving the clouds horizontally across the canvas, and ensuring they loop back to the beginning when they reach the edge of the canvas:

<br>

cloudX ++;

<br>

    if(cloudX > width){
      cloudX = 0;
    }

<br>

The cloudX++; operation increases the cloudX variable, moving the clouds to the right.

The if statement checks if the horizontal position of the clouds, is greater than the width of the canvas. If the clouds move beyond the right edge of the canvas, this condition becomes true. In this a case, the cloudX variable is reset to 0, moving the clouds back to the left edge of the canvas. This makes the clouds continuously loop across the canvas, creating the illusion of continuous movement.

<br>

This part of the code ensures that the spinning of the bird increases in speed:

<br>

    let timeElapsed = millis();

<br>

    let newFrameRate = map(timeElapsed, 0, 80000, 2, maxFrameRate);

<br>

    frameRate(newFrameRate);

<br>

let timeElapsed = millis(); captures the amount of time in milliseconds that has elapsed since the program started running.

let newFrameRate = map(timeElapsed, 0, 80000, 2, maxFrameRate); The map() function is used to convert the timeElapsed value from the range of 0 to 80000 milliseconds to a new range between 2 and maxFrameRate. This is adjusts the frame rate based on the elapsed time.

frameRate(newFrameRate); sets the frame rate of the sketch to the value calculated in the previous step.

<br>

This part of the code makes the bird spin around itself:

<br>

    let cir = 360 / num * (frameCount % num);
    rotate(radians(cir));

<br>

360 / num determines the angle for each bird.
frameCount % num calculates the remainder of dividing the current frame count by the number of birds.
rotate(radians(cir)) rotates the coordinate system by the angle cir expressed in radians.

<br>

**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**

Throbbers usually communicate a pause or delay in the expected interaction with a platform. They hide/simplify complex digital activity happening behind the scenes, and are supposed to reassure users that they are processing and working on a request or issue. When I encounter throbbers, while for instance watching movies and tv-shows, I am usually faced with frustration due to the lack of an estimated wait time, along with the throbber being dull and uninteresting to observe.

<br>

### **References**

[Reference for images](https://p5js.org/reference/#/p5/image)
<br>
[Reference for clouds](https://editor.p5js.org/mena-landry/sketches/D7ql4Nd3V)
