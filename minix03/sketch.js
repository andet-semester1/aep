// Declare variables for images
let img;
let treeImages = [];

// Set the number of birds to be drawn in the throbber
let num = 12;

// Define the maximum frame rate
let maxFrameRate = 90; 

// Initialize cloud coordinates
let cloudX = 0;

// Preload images before the program starts
function preload() {
  img = loadImage('bird.png'); // Load bird image
  treeImages.push(loadImage('summer.png')); // Load summer image
  treeImages.push(loadImage('autumn.png')); // Load autumn image
  treeImages.push(loadImage('winter.png')); // Load winter image
  treeImages.push(loadImage('spring.png')); // Load spring image
}

// Set up the canvas and initial configurations
function setup() {
  createCanvas(windowWidth, windowHeight); // Create canvas that matches window size
}

// Main draw loop, continuously executes the lines of code contained inside its block until the program is stopped
function draw() {
  // Create background with transparency
  background(173, 216, 230, 60); 

  // Draw clouds at various positions along the x-axis and y-axis
  for (let x = 0; x <= windowWidth; x += 400) {
    for (let y = 0; y <= windowHeight; y += 200) {
      drawCloud(cloudX + x, y);
    }
  }

  // Move clouds horizontally
  cloudX++;

  if(cloudX > width){
    cloudX = 0;
  }
  

  // Draw the current tree image
  let index = Math.floor((frameCount % 100) / 25);
  image(treeImages[index], 80, 150, 750, 750);

  push(); // Start a new drawing state

    // Calculate time elapsed since the program started
    let timeElapsed = millis();
    // Map the elapsed time to a frame rate value
    let newFrameRate = map(timeElapsed, 0, 80000, 2, maxFrameRate); // Adjust the time duration and the frame rate range
    
    // Set the new frame rate
    frameRate(newFrameRate); 

    translate(width / 1.4, height / 2); // Move the origin to the center for rotation
  
    // Calculate the degree of the bird's movement, changing its position over time
    let cir = 360 / num * (frameCount % num);

    rotate(radians(cir)); // Rotate the bird image
    image(img, 50, 10, 100, 100); // Draw the bird image

  pop(); // Restore original state
}

// Function to draw a cloud at specified coordinates
function drawCloud(x, y) {
  fill(250); // Set cloud color to white
  noStroke(); // Remove stroke
  ellipse(x, y, 70, 50); // Draw first part of the cloud
  ellipse(x + 10, y + 10, 70, 50); // Draw second part of the cloud
  ellipse(x - 20, y + 10, 70, 50); // Draw third part of the cloud
}