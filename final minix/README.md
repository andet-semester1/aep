# Final MiniX - Akuma Friend

Group 9, Aesthetic Programming, 06.06.2024
<br>
Characters: 15072

![](FinalMinixScreenshot1.png "billed titel")
<br>
![](FinalMinixScreenshot2.png "billed titel")
<br>

Please run the code [here](https://aesthetic-programming2981909.gitlab.io/gitlab/FinalProject/miniXFinalProject.html)
<br>
Please view the code [here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/FinalProject/miniXFinalProject.js?ref_type=heads)
<br>

**Introduction**
<br>
Our software is an interactive program designed to engage users in a dynamic question-and-answer experience with a virtual entity named Akuma. The program explores a scenario where a seemingly harmless virtual entity, initially portrayed as cute and friendly, gradually transitions into a malicious being that exploits the user's personal information. Akumas goal is to distract the user with personal questions, while “stealing” all the user’s data by “hacking” their computer. Akuma initially asks the user friendly, innocent questions such as “What is your favorite color?” seemingly just wanting to get to know the user better. As the interaction progresses, the questions gradually turn more ominous and invasive. Several of the interactions disregard the user's request to withhold personal information, accessing the video feed even if the user declines, and preventing the user from exiting the program. Meanwhile, Akuma persistently promotes the idea that it is a well-intentioned best friend by asking, "Who is your best friend?" and forcing the user to answer "Akuma" / “You” in order to progress. By the end of the questionnaire, the user uncovers the sinister truth when Akuma reveals the video feed again and announces that their identity and data have been stolen. While distracting the user with seemingly innocent questions, Akuma had been covertly “hacking” into their computer the entire time.

**The creation of the program**
<br>
Initially, global variables are declared to encompass elements such as background images (bg, bg0), the gifs we use to represent Akuma with (gif1, gif2), video capture (capture), as well as various variables that track visibility and those responsible for managing animations, typewriter effects, buttons, questions, and exit messages.

The program utilizes an if statement to transition from the start screen to the game screen. Within the draw function, nested if statements with boolean expressions as conditions, are employed to manage the visibility of questions, the wrong answer prompt, and exit messages, determining what should be displayed based on their status:

} else if (screen == 1) {
if (questionsVisible) {.... }
}

The JSON file contains two arrays (exitMessages and questions): exitMessages, which contains messages that are shown when the player attempts to exit the interaction, and a questions array. Each object in the questions array represents a question or statement that Akuma will present to the player. Each question object contains two properties: “question”, the text of the question or statement and “type”, the type of interaction.

When a question is displayed, the program uses the currentQuestion object to show the appropriate text and UI elements. For instance:

Type "text": Shows the question text and a continue button.
Type "free" or "special": Shows an input box for the user's answer.
Type "yesno": Shows Yes and No buttons.
Type "video": Displays a video feed from the user's camera.
Type “ending”: Initiates the ending of the program, displaying the dark version of Akuma and the continue button.

The showQuestions() function handles the logic to display the current question based on its type and updates the UI accordingly. Throughout the program, users are given two distinct ways of responding. Some questions feature an input box, allowing the user to type their own answers freely, while others are presented as simple “yes” or “no” questions.:

input = createInput('');
yesButton = createButton('Yes');
noButton = createButton('No');

By incorporating both open-ended and close-ended questions, the program creates variety and keeps the user more engaged and excited to answer Akuma's next question. If the user were only presented with close-ended questions, they might quickly become bored due to the repetitive and simplistic nature of these questions. This lack of variety would not require much thought, failing to keep the user sufficiently "distracted." If the user were given only open-ended questions, they might find it exhausting to continually type out and elaborate on their answers. This constant effort could lead to disengagement. By mixing both types of questions, the program maintains a dynamic and engaging experience for the user, while the responses influence the flow of the program, guiding users through several different interactions.

The p5.js built-in function “mousePressed” is attached to the button objects. The function works as an event listener, checking if the mouse is pressed on a specific button, and then executes the callback function “nextQuestion()” e.g. noButton.mousePressed(nextQuestion). Additionally, the program utilizes the "keyPressed()" function, enabling users to advance by hitting the enter key, but only when the input box is visible. This mechanism prevents users from bypassing the yes/no questions, ensuring a more thorough engagement with the program.

function keyPressed() {
if (inputVisible && keyCode === ENTER) { … }

These interactive elements enable users to progress through the program. The program doesn't record the user's actual answers to almost every question, as they don't have consequences. However, it creates the illusion that the responses matter. This is particularly evident with the one question of type: special, asking who the best friend of the user is, which triggers a specific response based on the user's input, and with Akuma's change in demeanor if the user attempts to exit.

The visual and stylistic choices of Akuma Friend are aimed to evoke a sense of mystery and subtle unease, establishing an atmosphere that feels simultaneously playful and unsettling. The program initially presents itself as an innocent game, greeting the user with gentle, calm music and a background featuring a green field and blue sky. Two contrasting versions of Akuma are introduced: one depicts Akuma as a cute, happy creature wearing cheerful yellow clothing, while the other portrays it as an ominous, shadowy figure with unsettling eyes. The latter version appears under specific circumstances: when the user answers a question incorrectly, at the end when the program's dark nature is unveiled, and as a glitchy effect if the user attempts to exit. This intentional "glitch" is strategically designed to deceive the user into believing it's an error in the game. During glitches, the dark version of Akuma appears for 0.05 seconds, represented by the glitchDuration variable set to 50 milliseconds. Glitches occur at intervals of every 1 second, defined by the glitchInterval variable set to the value of 1000 milliseconds.

let currentTime = millis();
if (currentTime - lastGlitchTime > glitchInterval) {
gif1.hide();
gif2.show();
setTimeout(function() {
gif2.hide();
gif1.show();
}, glitchDuration);
lastGlitchTime = currentTime;
}

The effect is utilized by checking if enough time has passed since the last glitch effect. If so, it triggers a glitch by hiding gif1 and showing gif2, and then reverting back after the specified duration (glitchDuration). The lastGlitchTime is updated to the current time to track when the last glitch occurred. In terms of animations and effects, the program also features elements such as text styled with certain fonts, a typewriter text effect and animated text movements to enhance the experience.

The program's title and name of the character, "Akuma," translates to "devil" in Japanese, emphasizing the dual nature of the character and implying that in the digital world, things are not always as they seem. Overall, the program sets out to explore themes of interactivity, data capture, and user engagement through a series of questions and surprising elements.

**Flowchart**
<br>
This flowchart provides a clear visual representation of the steps and decision points in the Akuma Friend process, highlighting critical decision points, loops, and conditions. It emphasizes the user experience, illustrating the flow of interactions and the application's semantics from the user's perspective. The primary goal is to structure and enhance the overall user experience.

![](FinalMinixFlowchart.png "billed titel")

**Themes and intersections of technical and cultural aspects of the code**
<br>
Our work addresses the theme “data capture” explored in chapter 4 in Aesthetic Programming, as well as datafication, a “broader cultural tendency” (Cox & Soon, 2020, p.99). One major way we incorporate this in our program is by using DOM elements. The elements we use that are a part of the DOM include buttons and text input. For example, in the keyPressed() function, it checks if the Enter key is pressed when an input field is visible. When the Enter key is pressed, it captures the user's input. In light of this, “keyboard capture” (Cox & Soon, 2020, p.105) plays a crucial role in our interaction process, allowing the program to register responses: The program captures user responses to questions asked during the interaction. Similarly, buttons play a central role in our program, serving as pivotal interactive elements that drive user engagement. We employ various buttons, including yesButton, noButton, continueButton, and exitButton, for the user to navigate through the program. Each button press represents an act of data capture, recording the user's choices and guiding the flow of the interaction. As noted in Aesthetic Programming, “in the era of big data, there appears to be a need to capture data from every action, even the most mundane ones like button pressing” (Cox & Soon, 2020, p.99), and the buttons in our program exemplify this trend. In the context of our program, buttons are particularly engaging because of the immediate response and sense of reward they provide, encouraging users to press them. This is similar to how buttons operate in software and online platforms like Facebook, where they prompt users to make binary decisions such as like or dislike, and accept or decline (Cox & Soon, 2020, p.99).

Furthermore, the integration of video capture extends the scope of datafication by incorporating visual data. This is achieved through the createCapture(VIDEO) function, which captures live video feed from the user's camera. While we do not store or analyze this video data, its real-time capture enhances user interaction and engagement, as it should result in a bit of shock for the user.

The captured data from these interactions, text inputs, button presses, and video feed, are processed and displayed within the program, though they are not stored or analyzed beyond the immediate interaction. In other words: The user’s input does not play a larger role in executing the program. The end-result of the program is the same nonetheless. The captured data reflects individual user interactions, contributing to the dynamic flow of the interactive experience. By using DOM elements for data capture, our program aligns with Terranova's concept of asymmetrical relations between active and receptive poles (Cox & Soon, 2020, p.116). The user, or active pole, engages with the program, or receptive pole, and their interactions are transformed into real-time responses that enhance the interactive experience. This process echoes Terranova's discussion on how individual actions, such as "liking and being liked, writing and reading, looking and being looked at," (Cox & Soon, 2020, p.116) become part of a larger network of social and technical interactions, even if the data is not collected or analyzed further. In simpler terms, it is like saying that every time we engage with something online, whether it is clicking a button, typing a message, or simply browsing, we are participating in a complex web of interactions that extend beyond our immediate actions. For further explanation: When interacting on for example a social media platform, the action does not disappear into a void, it becomes part of the platform’s data network. Similar to our program, even though the data capture from user interactions is not stored or analyzed, it still plays a role in enhancing the overall interactive experience.

**Akuma Friend as a critical work in and of itself**
<br>
The program can be considered a critical exploration, as it aims to explore broader societal concerns surrounding data privacy, surveillance and online trust. In an era where personal information is increasingly commercialized and exploited for various purposes, the idea touches upon the dangers of blindly sharing sensitive data with digital entities or individuals claiming to have good intentions online. It encourages users to thoughtfully consider the information they share with online platforms, additionally emphasizing the importance of exercising caution when downloading programs from the internet. Furthermore, we aimed to illustrate the ease with which others can access personal data, either directly or through hacking. This theme is particularly evident in the program's conclusion, where the malicious version of Akuma appears on the user's screen alongside video footage captured from the user's camera. Our work is inherently critical because the program can be considered unethical due to its portrayal of Akuma stealing the user’s data without their consent. The camera access is the primary method our program uses to convey to the user that their data has been “stolen” by Akuma.

The notion of a friendly facade masking ulterior motives resonates with real-world instances of online deception and manipulation. The “friend’s” ability to distract and retain the user's attention while forbidding them from exiting, highlights the level of control and irreversibility inherent in the digital traces left behind by every online interaction. The program initiates reflection and discussion upon the nature of personal and sensitive data, emphasizing how its definition can vary from one individual to another. Some might find themselves hesitant or suspicious, when asked to share answers to seemingly innocent questions such as "What is your name?", whereas others might perceive such inquiries as impersonal. The user being lured into a false sense of security only to have an experience of their data being stolen highlights the pervasive issue of surveillance and data harvesting digitally. The program's reveal of the virtual friend's true intentions serves as a reminder of the potential consequences of data collection and misuse, initiating reflection on the ethical implications of technology and the need for greater awareness of digital privacy rights.

Another critical aspect of our program is the appearance of the virtual friend. We aimed to design a character that maintains a sense of neutrality, avoiding any gendered associations such as being perceived as distinctly "male" or "female." To achieve this, we opted for a non-binary name for the character. Additionally, we selected a visual representation that does not resemble a human form, ensuring that the character's appearance does not conform to traditional gender norms or human characteristics.

<br>

### **References**

[Reference for change of screens](https://editor.p5js.org/ehersh/sketches/BkCYHm7Rm)
<br>
[Reference for fonts](https://editor.p5js.org/chanc245_chrissy/sketches/38M4tNTKd)
<br>
[Reference for bounce](https://editor.p5js.org/hz2788/sketches/hSIP9xMCF)
<br>
[Reference for inserting GIF](https://editor.p5js.org/remarkability/sketches/tuTPZXym9)
<br>
[Reference for typewriter effect](https://editor.p5js.org/pippinbarr/sketches/bjxEfpiwS)
<br>
[Reference for music](https://pixabay.com/music/search/sinnesl%c3%b6schen/)

<br>

### **Appendix**

[Link to Astrid Winther’s repository](https://gitlab.com/astrid-ap/aestetisk-programmering)
<br>
[Link to Julie Bjerregård Lauritzen’s repository](https://gitlab.com/aesthetic-programming2981909/gitlab)
<br>
[Link to Kalisha Danchell’s repository](https://gitlab.com/andet-semester1/aep/-/tree/main?ref_type=heads)
<br>
[Link to Pia Le’s repository](https://gitlab.com/piangocle/ap2024/-/tree/main)
