# InstaFerence

<br>

![](minix4-screenshot1.png "billed titel")
![](minix4-screenshot2.png "billed titel")

<br>

Please run the code [here](https://andet-semester1.gitlab.io/aep/minix04/minix4index.html)

Please view the code [here](https://gitlab.com/andet-semester1/aep/-/blob/main/minix04/minix4sketch.js)

<br>

**Provide a title for and short the description of your work (1000 characters or less) as if you were going to submit it to the festival**

"InstaFerence" is a critical exploration of the pervasive data capture embedded within social media platforms. By mimicking the interface of Instagram, the artwork aims to expose the mechanisms of data collection behind the scenes, through the users interactions with social media content. Each like, comment, and share contributes to the algorithm's construction of a user profile, and capture of personal information such as location, likes/dislikes and social networks. This algorithm shapes the content presented to you and can reinforce societal assumptions. The vibrant, innocent colors symbolize the naivety and ignorance often associated with users' engagement on these platforms, set in contrast to the user's camera feed suddenly displayed as the Instagram post- resembling a transparency of the pervasive surveillance of the user. One might perceive themselves as private by refraining from posting personal content. However, merely interacting with others' posts inadvertently leaves a trail of personal/private data for the algorithm to capture and perhaps share.

<br>

**Describe your program and what you have used and learnt.**

The program resembles an interactive simulation of the social media platform Instagram, with a cheerful post of a dog being displayed. Three recognizable buttons are present underneath the post. When the "like", "comment" or "share" button are clicked repeatedly, pictures in relation to data capturing will be displayed all over the screen, while an unexpected picture of yourself is captured and "posted" on the platform. Through the creation of the code, I learned how to manipulate images into buttons with likeButton = createImg('heart-outline.png') statements. I expanded my knowledge in regards to the possibilities of p5.js, for instance by capturing certain keys being pressed, and adding capture = createCapture(VIDEO), allowing me to access the video feed of the "user". I've become better at managing multiple functions and linking them to certain buttons. I've gained experience with pushing images into arrays, and displaying a random image/index when the buttons are pressed.

<br>

**How your program and thinking address the theme of “capture all.”**

By simulating user interactions on a familiar social media platform like Instagram, my program highlights how every interaction contributes to the capture and accumulation of personal data, hereby shaping the digital identity of individuals. My program enables "users" to reflect on the extent to which their online activities are monitored and collected by algorithms.

<br>

**What are the cultural implications of data capture?**

On one hand, the capture of data enables personalized experiences and content. It can enhance the satisfaction of user engagement. However, it can also raise concerns about privacy invasion, surveillance and biases in the algorithm. Data capture can establish echo chambers, reinforcing beliefs and biases, limiting the perspectives of minorities. The capture of data also raises concerns about ethical dilemmas regarding consent and ownership.

<br>

### **References**

[Reference for input box](https://p5js.org/reference/#/p5/input)
<br>
[Reference for image-button](https://gist.github.com/lizzybrooks/54045563e4e8321718cc40297db999f9)
