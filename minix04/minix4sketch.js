// Declaring global variables for the images, buttons, video capture, and data array
let instalogo;
let locationimg;
let adimg;
let soundimg;
let instapost;

let likeButton;
let commentButton;
let shareButton;

let capture;

let dataArray = [];

// Initially setting the visibility of the input box to false
let inputVisible = false; 

// Preload function to load images and video capture
function preload() {
  // Load images into variables
  instalogo = loadImage('Logo-Instagram.png');
  locationimg = loadImage('location2.png');
  adimg = loadImage('ad.png');
  soundimg = loadImage('sound.webp');
  instapost = loadImage('instapost1.jpeg');

  // Pushes 3 images into data array
  dataArray.push(locationimg);
  dataArray.push(adimg);
  dataArray.push(soundimg);

  // Create video capture and hide the default display
  capture = createCapture(VIDEO);
  capture.size(510, 510);
  capture.hide();
}

function setup() {
  // Create a canvas that matches the window size
  createCanvas(windowWidth, windowHeight);

  // Create and position the "like" button
  likeButton = createImg('heart-outline.png');
  likeButton.position(480, 655);
  likeButton.size(130, 130);
  likeButton.mousePressed(showYouLike);

  // Create and position the "comment" button
  commentButton = createImg('comment.webp');
  commentButton.position(660, 660);
  commentButton.size(112, 112);
  commentButton.mousePressed(showYouComment);

  // Create and position the "share" button
  shareButton = createImg('dm.png');
  shareButton.position(845, 675);
  shareButton.size(90, 90);
  shareButton.mousePressed(showYouShare);

  // Create the input field and hide it initially
  input = createInput('');
  input.position(520, 580);
  input.size(400, 30);
  input.hide();
}

function draw() {
  // Set the background color
  background(255, 182, 193);

  // Draw a white rectangle
  noStroke();
  fill(255);
  rect(450, 0, 550, 900);

  // Display the Instagram logo and initial post
  image(instalogo, 480, 15, 200, 100);
  image(instapost, 469, 120, 510, 510);
  image(img, 480, 630, 150, 150);
}

// Function to display video feed
function displayVideoFeed() { 
  image(capture, 469, 120, 510, 510);
}

// When the "share" button is clicked
function showYouShare() {
  // Display the video feed
  displayVideoFeed();

  // Display a random image from the data array at a random position
  let randomIndex = floor(random(dataArray.length));
  let randomImage = dataArray[randomIndex];
  let randomX = random(width);
  let randomY = random(height);
  image(randomImage, randomX, randomY, 150, 150);
}

// When the "comment" button is clicked
function showYouComment() {
  // Display the video feed
  displayVideoFeed();

  // Show the input field
  input.show();
  inputVisible = true; 
}

// When the "like" button is clicked
function showYouLike() {
  // Display the video feed
  displayVideoFeed();

  // Display a random image from the data array at a random position
  let randomIndex = floor(random(dataArray.length));
  let randomImage = dataArray[randomIndex];
  let randomX = random(width);
  let randomY = random(height);
  image(randomImage, randomX, randomY, 150, 150);

  // Change the like button to appear as a red heart
  likeButton.attribute('src', 'heart.png');
}

// Function to handle key presses
function keyPressed() {
  // If the input box is visible and enter key is pressed
  if (keyCode === ENTER && inputVisible) {
    // Clear the input box and hide it
    input.value('');
    input.hide();

    // Display a random image from the data array at a random position
    let randomIndex = floor(random(dataArray.length));
    let randomImage = dataArray[randomIndex];
    let randomX = random(width);
    let randomY = random(height);
    image(randomImage, randomX, randomY, 150, 150);

    // Set input visibility to false
    inputVisible = false; 
  }
}

// Ensures that the canvas always fills the entire browser window
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
