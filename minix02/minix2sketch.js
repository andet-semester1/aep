// Variable for the button that changes the shape
let shapeButton;
// Array of possible shape types
let shapeType = ["heart", "triangle", "circle", "square"];
// Index to track the current shape
let currentShapeIndex = 0;

// Variable for the button that changes the color
let colorButton;
// Array of possible colors
let colors = ['#FF6347', '#FF8C00', '#00FFFF', '#7FFF00', '#FF00FF', '#000000', '#FFFFFF']; 
// Index to track the current color
let currentColorIndex = 0;

// Variables for buttons that change the first emoji
let emoji1Button;
// Array of possible first emoji types
let emoji1Type = ["happy1", "sad1", "surprise1"];
// Index to track the current first emoji
let currentEmoji1Index = 0;

// Variables for buttons that change the second emoji
let emoji2Button;
// Array of possible second emoji types
let emoji2Type = ["flower2", "moon2", "sun2"];
// Index to track the current second emoji
let currentEmoji2Index = 0;

// Variables for buttons that change the third emoji
let emoji3Button;
// Array of possible third emoji types
let emoji3Type = ["unicorn3", "skull3", "alien3"];
// Index to track the current third emoji
let currentEmoji3Index = 0;

// Variables for buttons that change the fourth emoji
let emoji4Button;
// Array of possible fourth emoji types
let emoji4Type = ["happy4", "sad4", "angry4"];
// Index to track the current fourth emoji
let currentEmoji4Index = 0;

function setup() {
  // Create the canvas to fit the window size
  createCanvas(windowWidth, windowHeight);

  // Set a color for the buttons
  let buttonCol = color(255, 105, 180);

  // Create and style the shape button
  shapeButton = createButton('Change Emoji Shape');
  shapeButton.position(10, 10);
  shapeButton.mousePressed(changeShape);
  shapeButton.style('background-color', buttonCol);

  // Create and style the color button
  colorButton = createButton('Change Color');
  colorButton.position(10, 40);
  colorButton.mousePressed(changeColor);
  colorButton.style('background-color', buttonCol);

  // Create and style the first emoji button
  emoji1Button = createButton('Emotion 1');
  emoji1Button.position(10, 70);
  emoji1Button.mousePressed(changeEmoji1);
  emoji1Button.style('background-color', buttonCol);

  // Create and style the second emoji button
  emoji2Button = createButton('Emotion 2');
  emoji2Button.position(10, 100);
  emoji2Button.mousePressed(changeEmoji2);
  emoji2Button.style('background-color', buttonCol);

  // Create and style the third emoji button
  emoji3Button = createButton('Emotion 3');
  emoji3Button.position(10, 130);
  emoji3Button.mousePressed(changeEmoji3);
  emoji3Button.style('background-color', buttonCol);

  // Create and style the fourth emoji button
  emoji4Button = createButton('Emotion 4');
  emoji4Button.position(10, 160);
  emoji4Button.mousePressed(changeEmoji4);
  emoji4Button.style('background-color', buttonCol);
}

function draw() {
  // Set the background color
  background(255, 182, 193);
  noStroke();

  // Get the current shape and color
  let currentShape = shapeType[currentShapeIndex];
  fill(colors[currentColorIndex]);

  // Draw the current shape
  if (currentShape === 'heart') {
    circle(279, 250, 230); 
    circle(480, 250, 230); 
    triangle(590, 290.4, 365, 600, 170, 290.4);

  } else if (currentShape === 'triangle') {
    triangle(370, 90, 125, 500, 640, 500);

  } else if (currentShape === 'circle') {
    circle(370, 335, 400); 

  } else if (currentShape === 'square') {
    square(175, 145, 400);
  }

  // Get and draw the current first emoji
  let currentEmoji1 = emoji1Type[currentEmoji1Index];
  fill(255, 215, 0);

  if (currentEmoji1 === 'happy1') {
    // Face
    circle(320, 280, 70); 

    // Eyes and mouth
    fill(0);
    arc(318, 292, 50, 30, 0, PI);
    circle(305, 270, 10);
    circle(330, 270, 10); 

  } else if (currentEmoji1 === 'sad1') {
    // Face
    circle(320, 280, 70); 

    // Eyes and mouth
    fill(0);
    arc(318, 300, 50, 10, PI, TWO_PI);
    circle(305, 270, 10);
    circle(330, 270, 10); 

    // Eyebrows
    rect(300, 255, 12, 2);
    rect(325, 255, 12, 2);

  } else if (currentEmoji1 === 'surprise1') {
    // Face
    circle(320, 280, 70); 

    // Eyes and mouth
    fill(255, 255, 255);
    circle(305, 270, 20);
    circle(330, 270, 20); 
    fill(0, 0, 0);
    circle(305, 270, 10);
    circle(330, 270, 10); 
    circle(320, 300, 20); 
  }

  // Get and draw the current third emoji
  let currentEmoji3 = emoji3Type[currentEmoji3Index];

  if (currentEmoji3 === 'unicorn3') {
    // Horn
    fill(255, 20, 147);
    triangle(310, 360, 370, 325, 310, 400); 

    // Face
    fill(221, 160, 221);
    circle(320, 380, 70); 
    rect(320, 380, 50, 30);
    triangle(340, 360, 300, 330, 285, 380); 

    // Eye and nose
    fill(0, 0, 0);
    circle(330, 380, 15); 
    circle(360, 388, 10); 
    fill(221, 160, 221);
    circle(358, 390, 10); 

  } else if (currentEmoji3 === 'skull3') {
    // Face
    fill(192, 192, 192);
    circle(320, 380, 70); 
    rect(305, 395, 30, 30);

    // Eyes
    fill(0, 0, 0);
    circle(335, 380, 20);
    circle(305, 380, 20);
    fill(192, 192, 192);
    circle(320, 368, 30);

  } else if (currentEmoji3 === 'alien3') {
    // Face
    fill(60, 179, 113);
    circle(320, 380, 70); 
    triangle(290, 399, 350, 399, 320, 430); 

    // Eyes and mouth
    fill(0);
    circle(335, 380, 25);
    circle(305, 380, 25);
    arc(319, 410, 15, 5, 0, PI);
    fill(255);
    circle(335, 380, 5);
    circle(305, 380, 5);
  }

  // Get and draw the current fourth emoji
  let currentEmoji4 = emoji4Type[currentEmoji4Index];
  fill(255, 215, 0);

  if (currentEmoji4 === 'happy4') {
    // Face
    circle(420, 380, 70); 

    // Mouth
    fill(255, 255, 255);
    arc(418, 385, 50, 50, 0, PI);

    // Eyes
    fill(0, 0, 0);
    circle(405, 370, 10);
    circle(430, 370, 10); 
    fill(255, 215, 0);
    circle(405, 375, 10);
    circle(430, 375, 10); 

  } else if (currentEmoji4 === 'sad4') {
    // Face
    circle(420, 380, 70); 

    // Eyes and mouth
    fill(0, 0, 0);
    arc(418, 399, 50, 40, PI, TWO_PI);
    circle(405, 370, 10);
    circle(430, 370, 10); 

    // Tears
    fill(135, 206, 235);
    rect(400, 373, 9, 30);
    rect(425, 373, 9, 30);

  } else if (currentEmoji4 === 'angry4') {
    // Face
    circle(420, 380, 70); 

    // Eyes
    fill(178, 34, 34);
    circle(405, 380, 10);
    circle(430, 380, 10); 

    // Mouth
    fill(0, 0, 0);
    arc(418, 400, 50, 10, PI, TWO_PI);

    // Eyebrows
    push();
    translate(400, 360); 
    rotate(PI / 4); 
    rect(0, 0, 15, 5);
    pop();

    push();
    translate(425, 370); 
    rotate(-PI / 4);
    rect(0, 0, 15, 5); 
    pop(); 
  }

  // Get and draw the current second emoji
  let currentEmoji2 = emoji2Type[currentEmoji2Index];

  if (currentEmoji2 === 'flower2') {
    fill(139, 0, 139);
    translate(420, 280);
    for (let i = 0; i < 10; i++) {
      ellipse(0, 4, 15, 80);
      rotate(PI / 5);
    }

  } else if (currentEmoji2 === 'moon2') {
    fill(250, 235, 215);
    circle(420, 280, 70); 
    fill(colors[currentColorIndex]);
    circle(445, 280, 70); 

  } else if (currentEmoji2 === 'sun2') {
    fill(255, 165, 0);
    circle(420, 280, 76); 

    fill(255, 140, 0);
    circle(420, 280, 68); 

    fill(255, 255, 0);
    circle(420, 280, 60); 
  }
}

// Function to change the shape
function changeShape() {
  currentShapeIndex = (currentShapeIndex + 1) % shapeType.length;
}

// Function to change the color
function changeColor() {
  currentColorIndex = (currentColorIndex + 1) % colors.length;
}

// Function to change the first emoji
function changeEmoji1() {
  currentEmoji1Index = (currentEmoji1Index + 1) % emoji1Type.length;
}

// Function to change the second emoji
function changeEmoji2() {
  currentEmoji2Index = (currentEmoji2Index + 1) % emoji2Type.length;
}

// Function to change the third emoji
function changeEmoji3() {
  currentEmoji3Index = (currentEmoji3Index + 1) % emoji3Type.length;
}

// Function to change the fourth emoji
function changeEmoji4() {
  currentEmoji4Index = (currentEmoji4Index + 1) % emoji4Type.length;
}