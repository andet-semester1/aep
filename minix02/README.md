# Signature Emoji

**Describe your program and what you have used and learnt.**
<br>
I have created an interactive program, that enables the user to customize an emoji consisting of- and embracing several emotions, symbols and aesthetics. With two buttons the color and shape of the overall demeanor of the emoji can be changed, while four additional buttons determine the combination of emotions the user chooses to identify with and present.

In the creation of this code, I have learnt a lot about creating interactive buttons, as well as creating and calling several functions. The code begins with the determination of variables for each button, and assigning multiple values to them through arrays. I use currentEmoji3Index=0; to keep track of which one of the emojis/shapes are currently displayed. In the draw() function, the different shapes and emojis are drawn on the canvas depending on the value of the current…Index, using if () { } else if () { } statements. I have created several functions that update the indexes tracking the selected shape, color, and emoji type when their respective buttons are clicked: currentShapeIndex = (currentShapeIndex + 1) % shapeType.length;

**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?**
<br>
It can be argued that the way emojis portray real, complex human-emotions are generalizing and stereotypical. The aim of my program is to create a template that enables people to customize their own personalized signature emoji; a single emoji which manages to accommodate and embrace several emotions at once. By combining these otherwise one-sided emojis, new unique meanings can emerge, through which people can express their complex emotions, core values, identity and aesthetic by adding this emoji to their messages. With the program I take into account, that feelings and values can be expressed through colors and symbols, and that few people tend to express an emotion in the same way. I included the ability to portray the experience of feeling two conflicting emotions at once. This program is a suggestion as to how our interaction with emojis could be made more inclusive and personalized.

![](emojiscreenshot.png "billed titel")
<br>

Please run the code [here](https://andet-semester1.gitlab.io/aep/minix02/minix2index.html)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

<br>
Please view the full repository [here](https://gitlab.com/andet-semester1/aep/-/tree/main/minix02)
<br>
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->

[Reference for flower](https://p5js.org/examples/hello-p5-simple-shapes.html)
<br>
[Reference for triangles](https://p5js.org/reference/#/p5/triangle)
<br>
[Reference for rectangles](https://p5js.org/reference/#/p5/rect)
