//defining images
let backgroundimg;
let deadimg;
let hungryimg;
let tiredimg;
let boredimg;
let happy1img;
let happy2img;
let happy3img;
let playingimg;
let eatingimg;
let sleepingimg;

//defining array of happy images
let happyArray = [];

//defining buttons
let foodButton;
let playButton;
let sleepButton;

// variable to keep track of the previous state Grisse was in
let previousState = false;
// variable to hold the current image being displayed
let currentImage;

// variable to hold the image displayed when a button is pressed
let actionImage;

// boolean variable to control whether to display happy/want images 
let showOtherImages = true;

// variable to represent the pet object
let grisse;

// variable to keep track of the game over state
let gameOver = false;

class Pet{
  // initializing pet's energy, hunger, and fun levels to 50
  constructor() {
      this.energy = 50; 
      this.hunger = 50;
      this.fun = 50;
  }

  // method to simulate playing, decreases energy and increases fun
  play() {
      this.energy = this.energy - 5;
      this.fun = this.fun + 15;
  }

  // method to simulate eating, increases hunger and decreases energy
  eat() {

      this.hunger = this.hunger + 15;
      this.energy = this.energy - 5;

  }

  // method to simulate sleeping, increases energy and decreases hunger and fun
  sleep (){
    this.energy = this.energy + 15;
    this.hunger = this.hunger - 5;
    this.fun = this.fun - 5;
  }

}

function preload() {
   // loading background image
  backgroundimg = loadImage('background.jpeg');

  // loading images for different pet states
  deadimg = loadImage('dead.png')
  hungryimg = loadImage('hungry.gif')
  tiredimg = loadImage ('tired.gif')
  boredimg = loadImage ('bored.gif')
  happy1img = loadImage ('happy.gif')
  happy2img = loadImage ('happy2.gif')
  happy3img = loadImage ('happy3.gif')
  eatingimg = loadImage ('eating.gif')
  playingimg = loadImage ('play2.gif')
  sleepingimg = loadImage ('sleep.gif')

  // adding happy images to the happyArray
  happyArray.push(happy1img);
  happyArray.push(happy2img);
  happyArray.push(happy3img);

}


function setup() {
  createCanvas(windowWidth, windowHeight);


  // creating buttons for feeding, playing and sleeping
  foodButton = createImg('foodicon.png');
  foodButton.position(380, 30);
  foodButton.size (100,100)
  foodButton.mousePressed(grisseEating);

  playButton = createImg('playicon.png');
  playButton.position(370, 140);
  playButton.size (100,100)
  playButton.mousePressed(grissePlaying);

  sleepButton = createImg('sleepicon.png');
  sleepButton.position(370, 240);
  sleepButton.size (110,110)
  sleepButton.mousePressed(grisseSleeping);

  // creating a new pet object
  grisse = new Pet();
}

function draw() {
  background(255, 240, 245); 
  image(backgroundimg, 340, 0, 850, 850);

    // decreasing Grisse's nedds over time
    if (frameCount % 60 === 0) {
      grisse.energy -= 1;
      grisse.hunger -= 1;
      grisse.fun -= 1;
    }

  // drawing health bars
  drawHealthBars();

  // display action image if there is one/if a button has been pressed
  if (actionImage) {
    image(actionImage, 490, 250, 530, 530);
  }

  // drawing other images representing Grisse's state if showOtherImages is true
  if (showOtherImages) {

    // check if Grisse is happy
    let isHappy = grisse.hunger > 25 && grisse.hunger < 100 && grisse.energy > 25 && grisse.fun > 25;
    if (isHappy) {
      // check if Grisse was previously in a needy state
      if (!previousState) {
        // choose a random happy image from the happyArray
        let randomIndex = floor(random(happyArray.length));
        currentImage = happyArray[randomIndex];
        previousState = true; // update previous state
      }
    } else {
      previousState = false; 
    }

    // displaying Grisse's needy state based on priority
    if (grisse.hunger > 0 && grisse.hunger < 35) {
      image(hungryimg, 420, 250, 550, 550);
    } else if (grisse.energy > 0 && grisse.energy < 35) {
      image(tiredimg, 470, 380, 500, 500);
    } else if (grisse.fun > 0 && grisse.fun < 35) {
      image(boredimg, 500, 240, 540, 540);
    } else {
      image(currentImage, 500, 250, 550, 550); // display current happy image
    }

  }

      // displaying game over screens if Grisse's needs reach critical levels
      if (grisse.hunger <= 0 || grisse.energy <= 0 || grisse.fun <= 0 || grisse.hunger >= 100) {
        gameOver = true; // set gameOver to true once
        background (10);

        // hide buttons when the game is over
        foodButton.hide();
        playButton.hide();
        sleepButton.hide();

        // display dead image and different game over messages
        image(deadimg, 480, 330, 450, 450);
        noStroke();
        fill(255, 192, 203);
        rect(400, 230, 700, 100);
        textSize(60);
        fill(255, 20, 147);
  
        if (grisse.hunger <= 0) {
          text('Grisse starved to death.', 430, 300);
        } else if (grisse.energy <= 0) {
          textSize(50);
          text('Grisse was too tired and died.', 430, 300);
        } else if (grisse.fun <= 0) {
          text('Grisse died of boredom.', 430, 300);
        } else if (grisse.hunger >= 100) {
          text('Grisse was overfed.', 430, 300);
        }
  
        // show game over screen
        let gameOverScreen = new GameOverScreen();
        gameOverScreen.show();
      }

}

// function to handle Grisse eating
function grisseEating() {
  actionImage = eatingimg; // set action image to eating image
  grisse.eat(); // call the eat method of the grisse object
  showActionImage(); // call function to show action image
}

// function to handle Grisse playing
function grissePlaying() {
  actionImage = playingimg; // set action image to playing image
  grisse.play(); // call the play method of the grisse object
  showActionImage(); // call function to show action image
}

// function to handle Grisse sleeping
function grisseSleeping() {
  actionImage = sleepingimg; // set action image to sleeping image
  grisse.sleep(); // call the sleep method of the grisse object
  showActionImage(); // call function to show action image
}

// Function to show action image and clear it after 2 seconds
function showActionImage() {
  showOtherImages = false; // prevent showing other images temporarily
  setTimeout(() => {
    actionImage = null; // clear action image after 2 seconds
    showOtherImages = true; // allow showing other images again
  }, 2000); 
}


// function to draw health bars
function drawHealthBars() {
  // health bar colors
  let hungerColor = color(128,0,128); 
  let energyColor = color(238,130,238); 
  let funColor = color(186,85,211); 

  // bar lengths at max 100 points
  let maxBarLength = 200; 

  // calculate width of hunger bar
  let hungerBarWidth = map(constrain(grisse.hunger, 0, 100), 0, 100, 0, maxBarLength);
  fill(255);
  rect(500, 70, maxBarLength, 20); 
  fill(hungerColor);
  rect(500, 70, hungerBarWidth, 20);

  // calculate width of energy bar
  let energyBarWidth = map(constrain(grisse.energy, 0, 100), 0, 100, 0, maxBarLength);
  fill(255); 
  rect(500, 285, maxBarLength, 20);
  fill(energyColor);
  rect(500, 285, energyBarWidth, 20);

   // calculate width of fun bar
  let funBarWidth = map(constrain(grisse.fun, 0, 100), 0, 100, 0, maxBarLength);
  fill(255); 
  rect(500, 195, maxBarLength, 20);
  fill(funColor);
  rect(500, 195, funBarWidth, 20);
}