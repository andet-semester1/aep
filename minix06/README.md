# Grisse

<br>

![](screenshotminix6.png "billed titel")

<br>

Please run the code [here](https://andet-semester1.gitlab.io/aep/minix06/minix6index.html)

Please view the code [here](https://gitlab.com/andet-semester1/aep/-/blob/main/minix06/minix6sketch.js)

<br>

**Describe how does/do your game/game objects work?**

My game revolves around taking care of a virtual pet named Grisse. Grisse has attributes such as energy, hunger, and fun, which are represented by health bars on the screen and visualized through different images displayed on the screen, depending on Grisses mood or state. The player can interact with Grisse using buttons to feed, play, or put to sleep.

<br>

**Describe how you program the objects and their related attributes, and the methods in your game.**

I created a Pet class: class Pet{ }, and added a new object to it: grisse = new Pet();
The Pet class has attributes such as energy, hunger, and fun, initialized in constructor:

constructor() {
this.energy = 50;
this.hunger = 50;
this.fun = 50;
}

They are modified by methods like play(), eat(), and sleep(). The methods simulate the actions of playing with Grisse, feeding it, and putting it to sleep. The game displays images to represent Grisse's different states, such as being hungry, tired, or happy. Interaction with the game is introduced through buttons that trigger the corresponding methods when clicked.

<br>

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

Object-oriented programming allows encapsulating data and behavior within objects, enabling modularity and code reuse. For instance the Pet class encapsulates Grisse's attributes and behaviors, providing a clear and organized structure for managing its state. Abstraction enables a focus on essential details of an object while hiding unnecessary complexity. For example, in the game players can interact with Grisse through methods like play(), eat(), and sleep() without needing to know the internal workings of the actions.

<br>

### **References**

[Reference for objects](https://p5js.org/examples/objects-objects.html)
