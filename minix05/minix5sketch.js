// Array to hold circle objects
let circles = [];
// Number of circles to create
let numCircles = 10;
// Maximum movement increment for circles
let maxIncrement = 2;

function setup() {
  // Create a canvas of size 800x800 pixels
  createCanvas(800, 800);
  // Set the background color to white
  background(255);
  
  // Initialize circles with random positions and colors
  for (let i = 0; i < numCircles; i++) {
    circles.push({
      x: random(width),            // Random x position within canvas width
      y: random(height),           // Random y position within canvas height
      directionX: random([1, -1]), // Random initial direction for x (1 for right, -1 for left)
      directionY: random([1, -1]), // Random initial direction for y (1 for down, -1 for up)
      color: color(random(255), random(255), random(255)) // Random color
    });
  }
}

function draw() {
  // Move and display each circle in the array
  for (let i = 0; i < circles.length; i++) {
    moveCircle(circles[i]);    // Update circle position
    displayCircle(circles[i]); // Display the circle on the canvas
  }
  
  // Check for collisions between circles
  checkCollisions();
}

// Function to update circle position
function moveCircle(circle) {
  // Increment or decrement x position based on direction
  if (circle.directionX === 1) {
    circle.x++; // Move right
  } else {
    circle.x--; // Move left
  }

  // Increment or decrement y position based on direction
  if (circle.directionY === 1) {
    circle.y++; // Move down
  } else {
    circle.y--; // Move up
  }
  
  // Bounce off the edges of the canvas
  if (circle.x < 0 || circle.x > width) {
    circle.directionX *= -1; // Reverse x direction if hitting left or right edge
  }
  if (circle.y < 0 || circle.y > height) {
    circle.directionY *= -1; // Reverse y direction if hitting top or bottom edge
  }
}

// Function to display each circle
function displayCircle(circle) {
  fill(circle.color);           // Set fill color based on circle's color property
  noStroke();                   // No outline
  ellipse(circle.x, circle.y, 20); // Draw circle at current x, y with diameter 20
}

// Function to check for collisions between circles
function checkCollisions() {
  for (let i = 0; i < circles.length; i++) {
    for (let j = i + 1; j < circles.length; j++) {
      let d = dist(circles[i].x, circles[i].y, circles[j].x, circles[j].y); // Calculate distance between two circles
      if (d < 20) {  // If circles are touching (diameter of 20)
        // Change colors of colliding circles to random colors
        circles[i].color = color(random(255), random(255), random(255));
        circles[j].color = color(random(255), random(255), random(255));
      }
    }
  }
}
