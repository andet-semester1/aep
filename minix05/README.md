# Auto-generator

<br>

![](minix5screenshot.png "billed titel")

<br>

Please run the code [here](https://andet-semester1.gitlab.io/aep/minix05/minix5index.html)

Please view the code [here](https://gitlab.com/andet-semester1/aep/-/blob/main/minix05/minix5sketch.js)

<br>

**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior? What role do rules and processes have in your work?**

The program has two main rules:

1. If two circles intersect, they change into a random color.
<br>
2. If a circle hits the border of the canvas, it bounces back by reversing its velocity in the direction it hit the boundary (x or y).

<br>

Over time, the program simulates a dynamic system where circles continuously move, collide, and bounce off the edges of the canvas. As time progresses: Circles move according to their velocities.Collisions between circles result in color changes, creating a constantly evolving visual display. Circles bounce off the canvas edges, maintaining their movement within the canvas boundaries. This creates an ongoing and unpredictable animation of colorful circles. Emergent behavior arises from the simple interaction rules between circles and their environment. The rule that circles change color upon collision introduces unpredictability and variation in the visual output. Each collision creates a new color pattern, making the system's behavior complex and hard to predict over time. The rule that circles bounce off the edges ensures that they remain within the canvas, continuously interacting with each other and the boundaries. This maintains the system's dynamism and prevents it from reaching a static state. Rules and processes define the behavior and interactions of the elements within the generative system. They ensure that the system remains dynamic and visually interesting over time.

<br>

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

My MiniX demonstrates the concept of an "auto-generator" by illustrating how simple rules can autonomously produce complex and evolving behavior:

The program sets initial conditions and rules, then allows the system to evolve without further intervention. This level of control highlights the balance between initial design and autonomous operation.Once initiated, the circles move and interact independently according to the predefined rules. This autonomy is central to the concept of auto-generative systems, where elements operate and evolve on their own. The theme emphasizes the power of simple rules in creating complex systems. It underscores how initial conditions and interaction rules can lead to dynamic, emergent behavior without direct control. 

<br>

### **References**

[Reference for noise()](https://p5js.org/reference/#/p5/noise)

<br>
[Reference for for loop](https://p5js.org/reference/#/p5/for)
